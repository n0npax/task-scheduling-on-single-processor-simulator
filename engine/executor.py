# -*- coding: utf-8 -*-
__author__="n0npax"

import time
from copy import deepcopy

from algorithms.brute_force import BruteForce
from algorithms.genetic_alghoritm import GeneticAlgorithm
from algorithms.random_alghoritm import RandomAlgorithm
from algorithms.simulated_annealing import SimulatedAnnealing
from algorithms.tabu_search import TabuSearch
from algorithms.particle_swarm import Particle_swarm
from engine.algorithms.task import Task
from parallel import run_pararell
from parser import Parser


def get_exec_time(func):
    def func_wrapper(*args, **kwargs):
        start=time.time()
        ret = func(*args, **kwargs)
        stop=time.time()
        return ret, abs(stop-start)
    return func_wrapper


class Engine(object):


    def load_tasks(self, file_name, top_file_name, size):
        self.parser = Parser(file_name, top_file_name, str(size))
        self.size = int(size)

    def get_new_tasks(self):
        tasks_list=[]
        [periods, weigths, deadlines], top_penalty = self.parser.get_next()
        for i in range(self.size):
             tasks_list.append(Task(i, periods[i], weigths[i], deadlines[i]))
        return tasks_list, top_penalty

    @get_exec_time
    @run_pararell
    def run_simmulated_annealing(self, shared_output, tasks, alfa, temperature, epsilon):
        tasks = deepcopy(tasks)
        algorithm = SimulatedAnnealing(tasks)
        shared_output.append( [algorithm.compute(alfa, temperature, epsilon)])
        return

    @get_exec_time
    @run_pararell
    def run_brute_force(self,shared_output, tasks):
        tasks = deepcopy(tasks)
        algorithm = BruteForce(tasks)
        shared_output.append( [algorithm.compute()])
        return

    @get_exec_time
    @run_pararell
    def run_random_algorithm(self, shared_output, tasks, working_time):
        tasks = deepcopy(tasks)
        algorithm = RandomAlgorithm(tasks)
        shared_output.append([ algorithm.compute(working_time)] )
        return

    @get_exec_time
    @run_pararell
    def run_tabu_search(self, shared_output, tasks, working_time, repeat_penalty_max, tabu_list_size):
        tasks = deepcopy(tasks)
        algorithm = TabuSearch(tasks, tabu_list_size)
        shared_output.append([ algorithm.compute(working_time, repeat_penalty_max)])
        return

    @get_exec_time
    @run_pararell
    def run_genetic_alghoritm(self, shared_output, tasks,  generations, representantives_number, evolution_probability):
        tasks = deepcopy(tasks)
        algorithm = GeneticAlgorithm(tasks)
        shared_output.append([ algorithm.compute(generations, representantives_number, evolution_probability)])
        return

    @get_exec_time
    @run_pararell
    def run_particle_swarm(self, shared_output, tasks, working_time):
        tasks = deepcopy(tasks)
        algorithm = Particle_swarm(tasks)
        shared_output.append([ algorithm.compute(working_time)] )
        return