# -*- coding: utf-8 -*-
__author__="n0npax"
#TODO REFACTOR


import random

from engine.algorithms.task import *;


def compute_random_permutation(permutation):
    random.shuffle(permutation)

def compute_evolution(permutation):
    i1 = random.randrange(0, len(permutation), 1)
    i2 = random.randrange(0, len(permutation), 1)
    permutation[i1], permutation[i2] = permutation[i2], permutation[i1]


def mix_representatives(r1, r2, probability=0.5):
    "reprodukcja osobników"
    R1=r1[:]    #kopie rodziców
    R2=r2[:]

    R3=[]    #potomek
    TMP1=[]  #cechy różne
    TMP2=[]  #cechy różne
    for i in range(len(R1)):
        if R1[i]==R2[i]:
            R3.append(R1[i])    #gdy rodzice maja wspólną ceche potomek ją dziedziczy
        else:
            R3.append((Task(0, 0, 0, 0)))   #gdy rodzice mają różne cechy nie dziedziczy żadnej z nich tylko wstaw znacznik
            TMP1.append(R1[i])  #zapamietaj różne cechy rodziców
            TMP2.append(R2[i])

    TMP3=[]

    while TMP1!=[] and TMP2!=[]:
        if random.random()<probability:
            TMP3.append(TMP1[0]);
            for i in range(len(TMP2)):
                if TMP1[0]==TMP2[i]:
                    TMP2.pop(i);
                    break;
            TMP1.pop(0);
        else:
            TMP3.append(TMP2[0]);
            for i in range(len(TMP1)):
                if TMP2[0]==TMP1[i]:
                    TMP1.pop(i);
                    break;
            TMP2.pop(0);

    "dopisz brakujące cechy"
    j=0
    for i in range(len(R1)):
        if R3[i].num==0 and R3[i].period==0 and R3[i].weight==0:
            R3[i]=TMP3[j]
            j+=1

    return R3;



