# -*- coding: utf-8 -*-
__author__="n0npax"

class Task(object):

    def __init__(self, j, p,w,d, c=0, v=0):
        self.num=int(j)
        self.period=int(p)
        self.weight=int(w)
        self.deadline=int(d)
        self.end = -1

    def get_penalty(self, end):
        self.end=end
        return (self.end-self.deadline)*self.weight if self.end>self.deadline else 0

    def __eq__(self, other):
        if type(self) == type(other):
            return self.num ==  other.num and \
            self.period == other.period and \
            self.weight == other.weight and \
            self.deadline == other.deadline
        else:
            return False

    def __str__(self):
        return "Task number: "+str(self.num)+" period: "+str(self.period)+" weight: "+str(self.weight)+" deadline: "+str(self.deadline)