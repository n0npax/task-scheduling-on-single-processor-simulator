# -*- coding: utf-8 -*-
__author__="n0npax"


class SchedulerAlgorithm(object):

    def compute_penalty(self, tasks):
        penalty, end = 0, 0
        for task in tasks:
            end+=task.period
            penalty+=task.get_penalty(end)
        return penalty

