# -*- coding: utf-8 -*-
__author__="n0npax"

import random
import math
import sys

from scheduler_alghoritm import SchedulerAlgorithm


class SimulatedAnnealing(SchedulerAlgorithm):

    def __init__(self,tasks):
        self.tasks = tasks[:]
        self.size = len(tasks)

    def compute_next(self):          
        new_permutation=self.tasks[:]     
        i1, i2 = random.randrange(0, self.size, 1),  random.randrange(0, self.size, 1)
        new_permutation[i1], new_permutation[i2] = new_permutation[i2], new_permutation[i1]
        return new_permutation

    def compute(self, alfa, temperature, epsilon):
        
        penalty=self.compute_penalty(self.tasks)
        best_penalty = sys.maxint
        best_tasks = self.tasks

        while temperature>epsilon:
            next_tasks = self.compute_next()
            delta = self.compute_penalty(next_tasks)-penalty
            
            if delta<0 or random.random() < math.exp(-delta / temperature):
                self.tasks = next_tasks
                penalty+=delta
                if penalty<best_penalty:
                    best_penalty=penalty
                    best_tasks = self.tasks[:]
            temperature*=alfa
        return best_penalty, best_tasks

