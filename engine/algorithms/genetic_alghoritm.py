# -*- coding: utf-8 -*-
__author__="n0npax"

import sys
from scheduler_alghoritm import SchedulerAlgorithm
from genetic_stuff import mix_representatives, compute_evolution, compute_random_permutation

class GeneticAlgorithm(SchedulerAlgorithm):

    def __init__(self,tasks):
        self.tasks = tasks[:]
        self.size = len(tasks)

    def compute(self, generations, representantives_number, evolution_probability=10):

        representatives_list=[]
        posterity_list=[]
        best_tasks = []
        tmp_permutations = self.tasks[:]
        best_penalty = sys.maxint

        for j in range(representantives_number):
            representatives_list.append(tmp_permutations[:])
            compute_random_permutation(tmp_permutations)

        while(generations>0):
            generations-=1;

            if self.compute_penalty(representatives_list[0]) < best_penalty:
                best_penalty = self.compute_penalty(representatives_list[0])
                best_tasks = representatives_list[0][:]

            for j in range(0,len(representatives_list)-1):
                posterity_list.append(mix_representatives(representatives_list[j], representatives_list[j + 1]))
                posterity_list.append(mix_representatives(representatives_list[j + 1], representatives_list[j]))

            for t1 in range(len(posterity_list)):
                for t2 in range(len(posterity_list)):
                    if posterity_list[t1]==posterity_list[t2] and not t1==t2:
                        compute_evolution(posterity_list[t1])

            for j in range(2,len(posterity_list),evolution_probability):
                compute_evolution(posterity_list[j])

            posterity_list.sort(key=lambda a: self.compute_penalty(a))

            representatives_list=posterity_list[:representantives_number]
            posterity_list=[]


        return best_penalty, best_tasks





