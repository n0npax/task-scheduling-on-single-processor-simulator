# -*- coding: utf-8 -*-
__author__="n0npax"

import sys, time, random
from scheduler_alghoritm import SchedulerAlgorithm
from genetic_stuff import compute_evolution
from genetic_stuff import mix_representatives

class Particle_swarm(SchedulerAlgorithm):

    def __init__(self,tasks):
        self.tasks = tasks[:]
        self.size = len(tasks)

        self.act_velocity = 3 # start velocity
        self.dec_velocity_timer = 1 # start velocity
        self.best_instance=self.tasks[:]
        self.best_instance_now = self.best_instance
        self.best_penalty = sys.maxint
        self.c1 = .75
        self.c2 = .75
        self.c3 = .75
        self.r1 = .5
        self.r2 = .5
        self.r3 = .5

    def compute(self, stop_time, swarm_num=15):
        swarm = [self.tasks] * swarm_num

        start_time = time.time()
        while abs(start_time-time.time()) < stop_time:

            if abs(start_time-time.time()) > self.dec_velocity_timer * stop_time * 0.1:
                self.dec_velocity_timer += 1
                self.act_velocity-=1
            swarm.sort(key=lambda a: self.compute_penalty(a))
            self.best_instance_now = swarm[0]
            for instance in swarm:
                instance = self.update_position(instance)
            for i in range(len(swarm)):
                for j in range(i+1, len(swarm)):
                    if swarm[i]==swarm[j]:
                        random.shuffle(swarm[j])

        return self.compute_penalty(self.best_instance), self.best_instance

    def update_position(self, instance):
        if random.random() < self.r1:
            compute_evolution(instance)
        instance = mix_representatives(self.best_instance, instance, self.c1)[:]
        self.check_best(instance)
        if random.random() < self.r2:
            compute_evolution(instance)
        instance = mix_representatives(self.best_move(instance), instance, self.c2)[:]
        self.check_best(instance)
        if random.random() < self.r3:
            compute_evolution(instance)
        instance = mix_representatives(self.best_instance_now, instance, self.c3)[:]
        for i in range(self.act_velocity):
            compute_evolution(instance)
            self.check_best(instance)
        self.check_best(instance)
        return instance

    def check_best(self, instance):
        if self.compute_penalty(instance) < self.best_penalty:
            self.best_penalty = self.compute_penalty(instance)
            self.best_instance = instance[:]


    def best_move(self, instance):
        tmp_best_penalty = sys.maxint
        tmp_best_location=[]
        for i in range(len(instance)):
            for j in range(i+1, len(instance)):
                tmp_instance = instance[:]
                tmp_instance[i], tmp_instance[j] = tmp_instance[j], tmp_instance[i]
                if self.compute_penalty(tmp_instance)<tmp_best_penalty:
                    tmp_best_penalty=tmp_instance[:]
                    tmp_best_location = tmp_instance[:]
                    self.check_best(tmp_best_location)
        return tmp_best_location
