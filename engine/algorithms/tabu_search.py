# -*- coding: utf-8 -*-
__author__="n0npax"

import random
import time
import sys

from scheduler_alghoritm import SchedulerAlgorithm

class TabuSearch(SchedulerAlgorithm):

    def __init__(self,tasks, tabu_list_len=15):
        self.tasks = tasks[:]
        self.size = len(tasks)
        self.tabu_moves = []
        self.tabu_list_len = tabu_list_len

    def compute_random_permutation(self, permutation):
        random.shuffle(permutation)
        return permutation

    def best_move(self, current_permutation ):

        best_permutation = current_permutation[:]
        best_penalty = self.compute_penalty(best_permutation)

        for i in range(len(best_permutation)):
            for j in range(i+1,len(best_permutation)):
                tmp_permutation = current_permutation[:]
                tmp_permutation[i], tmp_permutation[j] = tmp_permutation[j], tmp_permutation[i]

                try:
                    for tabu_move in self.tabu_moves:
                        if [ x.num for x in tmp_permutation ].index( tabu_move[0]) > [ x.num for x in tmp_permutation ].index(tabu_move[1] ):
                            raise Exception('banned move')
                except Exception as e:
                    continue

                if self.compute_penalty(tmp_permutation) < best_penalty :
                    best_permutation = tmp_permutation[:]
                    best_penalty = self.compute_penalty(tmp_permutation)
                    if [tmp_permutation[i], tmp_permutation[j]] not in self.tabu_moves:
                        self.tabu_moves.append([tmp_permutation[i].num ,tmp_permutation[j].num ])
                        if len(self.tabu_moves)>self.tabu_list_len:
                            self.tabu_moves.pop(random.randint(0,len(self.tabu_moves)-1))

        if best_permutation==current_permutation:
            return self.compute_random_permutation(current_permutation)
        return best_permutation

    def compute(self, working_time=60, repeated_penalty_max=15):
        best_penalty = sys.maxint
        best_tasks = self.tasks
        old_penalty = -1
        start_time=time.time()
        current_permutation = self.tasks[:]
        repeated_penalty_cnt=0
        

        while time.time()-start_time < working_time:
            current_permutation = self.best_move(current_permutation)
            penalty = self.compute_penalty(current_permutation)
            if penalty < best_penalty:
                best_penalty = penalty
                best_tasks = current_permutation[:]
            if old_penalty == penalty:
                repeated_penalty_cnt+=1
                if repeated_penalty_cnt==repeated_penalty_max:
                    current_permutation = self.compute_random_permutation(current_permutation)[:]
                    repeated_penalty_cnt = 0
                    penalty = self.compute_penalty(current_permutation)
            old_penalty = penalty
        return best_penalty, best_tasks

