# -*- coding: utf-8 -*-
__author__="n0npax"

import sys
import itertools

from scheduler_alghoritm import SchedulerAlgorithm


class BruteForce(SchedulerAlgorithm):

    def __init__(self,tasks):
        self.tasks = tasks
        self.size = len(tasks)

    def compute(self):
        best_penalty = sys.maxint
        best_tasks = self.tasks
        all_permutations = itertools.permutations(self.tasks)
        while True:
            try:
                permutation = all_permutations.next()
                if best_penalty>self.compute_penalty(permutation):
                    best_tasks = permutation[:]
                    best_penalty = self.compute_penalty(permutation)
            except:
                break
        return best_penalty, best_tasks

