# -*- coding: utf-8 -*-
__author__="n0npax"

import random
import time
import sys

from scheduler_alghoritm import SchedulerAlgorithm

class RandomAlgorithm(SchedulerAlgorithm):

    def __init__(self,tasks):
        self.tasks = tasks[:]
        self.size = len(tasks)

    def compute_random_permutation(self, permutation):
        random.shuffle(permutation)
        return permutation

    def compute(self, working_time=60):
        best_penalty = sys.maxint
        best_tasks = self.tasks
        start_time=time.time()
        current_permutation = self.tasks[:]

        while time.time()-start_time < working_time:
            self.compute_random_permutation(current_permutation)
            penalty = self.compute_penalty(current_permutation)
            if penalty < best_penalty:
                best_penalty = penalty
                best_tasks = current_permutation[:]
        return best_penalty, best_tasks

