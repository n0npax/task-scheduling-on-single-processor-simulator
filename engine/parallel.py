# -*- coding: utf-8 -*-
__author__="n0npax"

import __builtin__
import multiprocessing
from functools import wraps

def run_pararell(func):
    @wraps(func)
    def func_wrapper(*args, **kwargs):
        process_lst =[]

        if func.__name__ == 'run_brute_force': cores_num = 1
        else : cores_num = multiprocessing.cpu_count()
        print 'works on {0} cores {1}'.format(cores_num, func.__name__)

        for process_num in range(cores_num):
            process_lst.append(multiprocessing.Process(target = func, args = args, kwargs = kwargs))
            process_lst[process_num].start()
        for process_num in range(cores_num):
            process_lst[process_num].join()
        return process_lst
    return func_wrapper

