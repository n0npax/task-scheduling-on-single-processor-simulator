# -*- coding: utf-8 -*-
__author__="n0npax"
import matplotlib
import matplotlib.pyplot as plt
import numpy

import matplotlib.pyplot as plt
import matplotlib.patches as patches

def generate_order_graph(tasks, instance_num, penalty_delta):
    order_values = tuple([task.num for task in tasks])
    N = len(tasks)
    ind = numpy.arange(N)  # the x locations for the groups

    fig = plt.figure()

    ax = fig.add_subplot(111)
    ax.set_xlim([0,len(tasks)])
    ax.set_title("Best find order\n Instance {0} \n Delta penalty: {1} ".format(instance_num,penalty_delta))

    rects = ax.bar(ind, order_values, 1.0, linewidth=0, color='none')
    ax_height = 0

    generated_rectangles=[]
    act_end=0
    for task in tasks:
        ax_height = max(ax_height, task.period )

        for period in range(0,task.period):
            act_end+=1
            fc='blue'
            if act_end>task.deadline: fc='red'
            else: fc='green'
            generated_rectangles.append(
                patches.Rectangle(
                (task.num, period), 1, 1,
                linewidth=0,
                facecolor=fc)
    )
    ax.set_ylim([0,ax_height+1])

    for p in generated_rectangles:
        ax.add_patch(p)
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 0, '%d' % int(height), ha='center', va='bottom')


    plt.setp(ax.get_xticklabels(), visible=False)

    fig.savefig('generated_images/order_{0}.png'.format(instance_num), dpi=90, bbox_inches='tight')

def generate_graph(delta_v, time_v,labels_v, instance_num ):
    print "graph algorithms comparision"
    labels_v = [x.replace('_','\n') for x in labels_v]
    print("delta: ", delta_v)
    print("time: ",time_v)
    print("name: ",labels_v)
    print("instance :", instance_num )
    print("================")
    N = len(delta_v)
    delta_values = tuple(delta_v)
    delta_line = tuple([0.1]*N)
    time_values = tuple(time_v)

    time_lines = delta_line

    ind = numpy.arange(N)
    width = 0.4

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    rects1 = ax.bar(ind, delta_values, width, color='r', yerr=delta_line)

    rects2 = ax.bar(ind + width, time_values, width, color='y', yerr=time_lines)

    # add some text for labels, title and axes ticks
    ax.set_ylabel('Scores')
    ax.set_title("Instance : {0}".format(instance_num))
    ax.set_xticks(ind + width)
    ax.set_xticklabels(tuple(labels_v))

    ax.legend((rects1[0], rects2[0]), ('Delta', 'Time [s]'))


    def autolabel(rects):
        # attach some text labels
        for rect in rects:
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width()/2., 0.75*height, '%d' % int(height), ha='center', va='bottom')

    autolabel(rects1)
    autolabel(rects2)

    fig.savefig('generated_images/alghoritms_{0}.png'.format(instance_num), dpi=90, bbox_inches='tight')


#plt.savefig('myfig')
