# -*- coding: utf-8 -*-
__author__="n0npax"



class Parser(object):

    def __init__(self,name, top_name, size):
        _file_rules={ "10" : 3, "40": 6, "50" : 9, "100" : 15 }
        self.tasks=[]
        self.best_penalty=[]
        if size not in _file_rules: size = "10"
        step = int(_file_rules[size])/3
        with open(name,'r') as f:
            lines = f.readlines()
            for i in range(0, len(lines), step):
                self.tasks.append(list(" ".join(lines[i:i+step]).replace('\n','').split() ))
        self.tasks = [x for x in [ l for l in self.tasks[:][::-1] if l ] if x ]
        with open(top_name, 'r') as f:
            self.best_penalty = [ int(x.replace('\n','')) for x in f.read().replace('\n','').split() if x]


    def get_next(self):
        return [self.tasks.pop() for _ in [0,1,2]], self.best_penalty.pop(0)
            

