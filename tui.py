#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__="n0npax"

import sys
import dicttoxml
from xml.dom.minidom import parseString
import argparse
import multiprocessing
import engine.executor as executor

from engine.graphs import generate_graph, generate_order_graph

parser = argparse.ArgumentParser(
        description='Wroclaw university of technology. Project for SWDiSK',
        argument_default=argparse.SUPPRESS,
        formatter_class=argparse.RawTextHelpFormatter)

group_files = parser.add_argument_group('files')
group_files.add_argument('--instances_file', metavar='FILE', required=True)
group_files.add_argument('--top_penalties', metavar='FILE', required=True)

group_size = parser.add_argument_group('instance')
group_size.add_argument('--size', metavar='int', required=True, type=int, help="instance size")
group_size.add_argument('--instances_number', metavar='int', required=True, type=int, help="number of instance for processing")

group_files = parser.add_argument_group('research support')
group_files.add_argument('--xml_generate', action="store_true", help="generate xml files with generated data")
group_files.add_argument('--repeat', type=int, help="how many times run alghoritm", required=True)


group1 = parser.add_argument_group('Brute force')
group1.add_argument('--brute_force', help='Brute Force. Do not use on big instances')

group2 = parser.add_argument_group('Tabu Search')
group2.add_argument('--tabu_search', help='Tabu Search', action='store_true')
group2.add_argument('--tabu_time', help='how long should algorithm works', type=float)
group2.add_argument('--repeat_penalty_max', help='after N same best penalty jump to random position', type=int)
group2.add_argument('--tabu_list_size', help='size of tabu list', type=int)

group3 = parser.add_argument_group('Genetic Algorithm')
group3.add_argument('--genetic', dest='genetic', help='Genetic alghoritm', action='store_true')
group3.add_argument('--generations', help='generations number for algorithms', type=int)
group3.add_argument('--representantives_number', help='number of representatives in population' , type=int)
group3.add_argument('--evolution_probability', help='every N representative will evolute randomly', type=int)

group4 = parser.add_argument_group('simmulated annealing')
group4.add_argument('--simmulated_annealing', help='simmulated annealing', action='store_true')
group4.add_argument('--alpha', help='how fast temperature is going down. example 0.9995', type=float)
group4.add_argument('--epsilon', help='the lowest accetable temperature', type=float)
group4.add_argument('--temperature', help='starting temperature', type=float)

group5 = parser.add_argument_group('Random Algorithm')
group5.add_argument('--random_algorithm', help='Random Search', action='store_true')
group5.add_argument('--random_time', help='how long should algorithm works', type=float)

group6 = parser.add_argument_group('Particle Swarm')
group6.add_argument('--particle_swarm', help='particle swarm', action='store_true')
group6.add_argument('--swarm_time', help='how long should algorithm works', type=float)

def get_best_solution(delta_scores, times_scores, repeat_num):
    tmp_delta, tmp_time = [], []
    for i in range(repeat_num):
        tmp_delta.append(delta_scores.pop())
        tmp_time.append(times_scores.pop())
    delta_scores.append(min(tmp_delta))
    times_scores.append(tmp_time[0])

def colect_solutions_data(shared_output, delta_scores, best_permutation, top_penalty, key, best_perm_dict):
    tmp_delta_scores = []
    tmp_best_permutation = []
    for result in shared_output:
        tmp_delta_scores.append(result[0][0])
        tmp_best_permutation.append([result[0][1]])
    tmp_delta_scores.sort()
    #print key, " : :", [x - top_penalty for x in tmp_delta_scores ]
    delta_scores.append(min(tmp_delta_scores)-top_penalty)
    best_permutation_instance = tmp_best_permutation[tmp_delta_scores.index(min(tmp_delta_scores))]
    best_permutation.append(best_permutation_instance)
    best_permutation_instance = best_permutation_instance[0]
    if 'xml_generate' in args.keys():
        best_perm_dict[key] = {'delta' : min(tmp_delta_scores), 'penalty' : min(tmp_delta_scores)+top_penalty, 'tasks' :
            {'order_number_{0}'.format(i) :
                 { 'number' : best_permutation_instance[i].num, 'weight' : best_permutation_instance[i].weight,
                   'period' : best_permutation_instance[i].period, 'deadline' : best_permutation_instance[i].deadline }
                        for i in range(len(best_permutation_instance)) }}



if __name__ == "__main__":
    args = vars(parser.parse_args())
    manager = multiprocessing.Manager()

    e = executor.Engine()
    e.load_tasks(args['instances_file'],args['top_penalties'], args['size'])
    for instance_number in range(args['instances_number']):
        print "working on instance {0}".format(instance_number)
        best_perm_dict = {}
        delta_scores, times_scores, best_permutation = [], [], []
        for key in [key_checked for key_checked in ['particle_swarm', 'cuda', 'simmulated_annealing','genetic', 'tabu_search', 'brute_force', 'random_algorithm'] if key_checked in args]:

            shared_output = manager.list()

            tasks, top_penalty = e.get_new_tasks()
            if key=='genetic':
                for i in range(args['repeat']):
                    times_scores.append(e.run_genetic_alghoritm(shared_output, tasks, args['generations'], args['representantives_number'], args['evolution_probability']))
                    colect_solutions_data(shared_output, delta_scores, best_permutation, top_penalty, key, best_perm_dict)
                get_best_solution(delta_scores, times_scores, args['repeat'])

            elif key=='random_algorithm':
                for i in range(args['repeat']):
                    times_scores.append( e.run_random_algorithm(shared_output, tasks, args['random_time']))
                    colect_solutions_data(shared_output, delta_scores, best_permutation, top_penalty, key, best_perm_dict)
                get_best_solution(delta_scores, times_scores, args['repeat'])


            elif key=='tabu_search':
                for i in range(args['repeat']):
                    times_scores.append(e.run_tabu_search(shared_output, tasks, args['tabu_time'], args['repeat_penalty_max'], args['tabu_list_size']))
                    colect_solutions_data(shared_output, delta_scores, best_permutation, top_penalty, key, best_perm_dict)
                get_best_solution(delta_scores, times_scores, args['repeat'])

            elif key=='simmulated_annealing':
                for i in range(args['repeat']):
                    times_scores.append(e.run_simmulated_annealing(shared_output, tasks, args['alpha'], args['temperature'], args['epsilon']))
                    colect_solutions_data(shared_output, delta_scores, best_permutation, top_penalty, key, best_perm_dict)
                get_best_solution(delta_scores, times_scores, args['repeat'])

            elif key=='particle_swarm':
                for i in range(args['repeat']):
                    times_scores.append( e.run_particle_swarm(shared_output, tasks, args['swarm_time']))
                    colect_solutions_data(shared_output, delta_scores, best_permutation, top_penalty, key, best_perm_dict)
                get_best_solution(delta_scores, times_scores, args['repeat'])

            elif key=='brute_force':
                times_scores.append(e.run_brute_force(shared_output, tasks))
                colect_solutions_data(shared_output, delta_scores, best_permutation, top_penalty, key, best_perm_dict)

        best_permutation = best_permutation[delta_scores.index(min(delta_scores))]
        generate_order_graph(best_permutation[0],str(instance_number+1), min(delta_scores))
        generate_graph(delta_scores, [ times_scores[i][1] for i in range(len(times_scores)) ], [key_checked for key_checked in ['particle_swarm', 'simmulated_annealing','genetic', 'tabu_search', 'brute_force', 'random_algorithm'] if key_checked in args], str(instance_number+1))
        xml_value = dicttoxml.dicttoxml(best_perm_dict)

        if 'xml_generate' in args.keys():
            with open('generated_xmls/solution_{0}.xml'.format(instance_number),'w') as f:
                f.write(parseString(xml_value).toprettyxml())
            args['executed_cmd']=" ".join(sys.argv)
            xml_value = dicttoxml.dicttoxml(args)
            with open('generated_xmls/last_run.xml'.format(instance_number),'w') as f:
                f.write(parseString(xml_value).toprettyxml())
